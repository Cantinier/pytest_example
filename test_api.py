import pytest
import requests


def test_api_response_status():
    """Checking if the response code is 200"""
    url = "https://currate.ru/api/?get=rates&pairs=USDRUB&key=f4341c7431bd3dc49f4f6793967d823d"
    response = requests.get(url)
    assert response.status_code == 2000, "Код ответа сервера не равен 200"


def test_api_response_data():
    """Checking if the server response contains a currency pair"""
    url = "https://currate.ru/api/?get=rates&pairs=USDRUB&key=f4341c7431bd3dc49f4f6793967d823d"
    response = requests.get(url)
    data = response.json()
    assert "USDRUB" in data.get("data", {}) and data["data"]["USDRUB"] is not None,\
        'В ответе сервера отсутствует валютная пара USDRUB'
