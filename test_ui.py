import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture
def driver(request):
    # Инициализация драйвера Selenium
    options = Options()
    # options.add_argument("--headless")  # Запуск браузера в режиме без графического интерфейса
    driver = webdriver.Chrome(options=options)
    yield driver
    # Закрытие браузера после выполнения тестов
    driver.quit()


@pytest.fixture(autouse=True)
def initial_setup(driver):
    # Переход на страницу
    driver.get("https://currate.ru/account")




def test_email_field_present(driver):
    """Checking if an email field exists"""
    # Проверка наличия поля email
    email_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="email"]'))
    )
    assert email_field.is_displayed()


def test_password_field_present(driver):
    """Checking if an password field exists"""
    # Проверка наличия поля password
    password_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="password"]'))
    )
    assert password_field.is_displayed()


def test_email_field_fill(driver):
    """Validation of email fields"""
    # Заполнение поля email
    email_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="email"]'))
    )
    email_field.send_keys("test@example.com")

    # Проверка значения поля email
    assert email_field.get_attribute("value") == "test@example.com"


def test_password_field_fill(driver):
    '''Validation of password fields'''
    # Заполнение поля password
    password_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="password"]'))
    )
    password_field.send_keys("secretpassword")

    # Проверка значения поля password
    assert password_field.get_attribute("value") == "secretpassword"


def test_invalid_login(driver):
    """Checking if an error is displayed when an incorrect password is entered"""
    # Ввод неправильной почты и пароля
    email_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="email"]'))
    )
    email_field.send_keys("invalid@example.com")

    password_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="password"]'))
    )
    password_field.send_keys("invalidpassword")

    # Нажатие на кнопку "Вход"
    submit_button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[type="submit"]'))
    )
    submit_button.click()

    # Проверка отображения сообщения об ошибке
    error_message = WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, '.error'))
    )
    assert error_message.text == "Пользователь не найден"


def test_successful_login(driver):
    """Checking for Successful Authorization"""
    # Ввод определенной почты и пароля
    email_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="email"]'))
    )
    email_field.send_keys("chernyy_pg@outlook.com")

    password_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="password"]'))
    )
    password_field.send_keys("6474b87c7b1d2")

    # Нажатие на кнопку "Вход"
    submit_button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[type="submit"]'))
    )
    submit_button.click()

    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '.title'))
    )

    # Обновление страницы
    driver.refresh()

    # Проверка успешной авторизации
    api_key_element = WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, '.apikey'))
    )
    assert api_key_element.text == "f4341c7431bd3dc49f4f6793967d823d"
